import 'dart:io';

import 'package:badges/badges.dart';
import 'package:camera/camera.dart';
import 'package:camera_app/image_edit.dart';
import 'package:camera_app/main.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' show join;

class CameraPage extends StatefulWidget {
  final List<CameraDescription> camera;
  CameraPage(this.camera);

  @override
  _CameraPageState createState() => _CameraPageState();
}

IconData cameraDirectionIcon = Icons.camera_rear;

class _CameraPageState extends State<CameraPage> with WidgetsBindingObserver {
  CameraController cameraController;
  Future<void> initializeControllerFuture;
  List<String> imagePath = [];
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    cameraController = CameraController(
      widget.camera[0],
      ResolutionPreset.medium,
    );
    initializeControllerFuture = cameraController.initialize();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      cameraController?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (cameraController != null) {}
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            FutureBuilder<void>(
              future: initializeControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return CameraPreview(cameraController);
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 40),
                  padding: EdgeInsets.symmetric(vertical: 10),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white.withOpacity(0.6),
                  child: Center(
                    child: Text(
                      'Take a photo or video of your issue',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Icon(
                      Icons.portable_wifi_off,
                      size: 40,
                      color: Colors.white,
                    ),
                    SizedBox(width: MediaQuery.of(context).size.width / 1.8),
                    FloatingActionButton(
                      heroTag: 'btn1',
                      backgroundColor: Colors.white.withOpacity(0),
                      onPressed: () {
                        if (cameraController != null &&
                            cameraController.description.lensDirection ==
                                CameraLensDirection.back) {
                          onNewCameraSelected(cameras[1]);
                        } else {
                          onNewCameraSelected(cameras[0]);
                        }
                      },
                      child: Icon(
                        cameraDirectionIcon,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(width: 15),
                    Icon(
                      Icons.flash_auto,
                      size: 40,
                      color: Colors.white,
                    ),
                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        width: 80,
                        height: 130,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: (imagePath.length == 0)
                            ? null
                            : _ImageContainer(imagePath[imagePath.length - 1])),
                    FloatingActionButton(
                      heroTag: 'btn2',
                      child: Icon(Icons.camera_enhance),
                      onPressed: () async {
                        try {
                          await initializeControllerFuture;
                          final path = join(
                              (await getTemporaryDirectory()).path,
                              '${DateTime.now()}.png');

                          await cameraController.takePicture(path);
                          setState(() {
                            imagePath.add(path);
                          });
                        } catch (e) {
                          print(e);
                        }
                      },
                    ),
                    FloatingActionButton(
                      heroTag: 'btn3',
                      child: Icon(Icons.done_all),
                      onPressed: () async {},
                    ),
                  ],
                ),
                SizedBox(height: 30),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _ImageContainer(image) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ImageEdit(imagePath),
          ),
        );
      },
      child: Badge(
        badgeColor: Colors.white,
        badgeContent: Text(imagePath.length.toString()),
        toAnimate: false,
        position: BadgePosition.topLeft(),
        child: Image.file(File(image)),
      ),
    );
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (cameraController != null) {
      await cameraController.dispose();
    }
    cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
    );

    // If the controller is updated then update the UI.
    cameraController.addListener(() {
      if (mounted) setState(() {});
      if (cameraController.value.hasError) {}
    });

    try {
      await cameraController.initialize();
    } on CameraException catch (e) {}

    if (mounted) {
      setState(() {});
    }

    switch (cameraDescription.lensDirection) {
      case CameraLensDirection.back:
        setState(() {
          cameraDirectionIcon = Icons.camera_rear;
        });
        break;
      case CameraLensDirection.front:
        setState(() {
          cameraDirectionIcon = Icons.camera_front;
        });
        break;
      case CameraLensDirection.external:
        break;
    }
  }
}
