import 'dart:io';
import 'dart:ui';
import 'package:arrow_path/arrow_path.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_drawing/path_drawing.dart';

String image;
bool toDraw = false;
bool toMeasure = false;

class ImageEdit extends StatefulWidget {
  List<String> imagePath;
  ImageEdit(this.imagePath) {
    image = imagePath[0];
  }

  @override
  _ImageEditState createState() => _ImageEditState();
}

class _ImageEditState extends State<ImageEdit> {
  Offset pos1 = Offset(0, 0);
  Offset pos2 = Offset(0, 0);
  List<Offset> listPos = <Offset>[];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GestureDetector(
          behavior: HitTestBehavior.deferToChild,
          onPanUpdate: (DragUpdateDetails details) {
            setState(() {
              RenderBox box = context.findRenderObject();
              pos2 = box.globalToLocal(details.globalPosition);
              listPos = List.from(listPos)..add(pos2);
            });
          },
          onPanStart: (details) {
            RenderBox box = context.findRenderObject();
            pos1 = box.globalToLocal(details.globalPosition);
          },
          onPanEnd: (details) {},
          child: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Image.file(File(image)),
              ),
              Container(
                width: 40,
                height: 200,
                margin: EdgeInsets.all(20),
                color: Colors.black.withOpacity(0.4),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FloatingActionButton(
                      onPressed: (){
                        print('text');
                      },
                      child: Icon(
                        Icons.text_fields,
                        color: Colors.white,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('measure');
                        setState(() {
                          toMeasure = true;
                        });
                      },
                      child: Icon(
                        Icons.linear_scale,
                        color: Colors.white,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('draw');
                        // setState(() {
                        //   toMeasure = true;
                        // });
                      },
                      child: Icon(
                        Icons.brush,
                        color: Colors.white,
                      ),
                    ),
                    Icon(
                      Icons.refresh,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
              CustomPaint(
                painter: getWidget(listPos, pos1),
                child: Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

getWidget(listPos, pos1) {
  if (toDraw) {
    return Draw(listPos, pos1);
  } else if (toMeasure) {
    return Measure(listPos, pos1);
  } else {
    return null;
  }
}

class Measure extends CustomPainter {
  List<Offset> listPos;
  Offset pos;
  Measure(this.listPos, this.pos);

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    final paint = Paint();
    paint.color = Colors.greenAccent;

    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 4;
    var c = Offset(size.width / 2, size.height / 2);
    var path = Path();
    // path.lineTo(listPos[listPos.length - 1].dx, listPos[listPos.length - 1].dy);
    path.moveTo(pos.dx, pos.dy);
    path.lineTo(listPos[listPos.length - 1].dx, listPos[listPos.length - 1].dy);
    path = ArrowPath.make(path: path, isDoubleSided: true);
    canvas.drawPath(
      dashPath(path, dashArray: CircularIntervalList<double>(<double>[10, 10])),
      paint,
    );
  }

  @override
  bool shouldRepaint(Measure old) {
    // TODO: implement shouldRepaint
    return old.listPos != listPos;
  }
}

class Draw extends CustomPainter {
  List<Offset> listPos;
  Offset pos;
  Draw(this.listPos, this.pos);

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    final paint = Paint();
    paint.color = Colors.greenAccent;

    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 4;
    var c = Offset(size.width / 2, size.height / 2);
    PointMode pointMode = PointMode.points;

    for (var i = 0; i < listPos.length; i++) {
      canvas.drawPoints(pointMode, listPos, paint);
    }
  }

  @override
  bool shouldRepaint(Draw old) {
    // TODO: implement shouldRepaint
    return old.listPos != listPos;
  }
}
